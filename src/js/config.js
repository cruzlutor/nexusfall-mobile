/* jslint node: true */

'use strict';

// requires
module.exports = function($stateProvider, $locationProvider, $urlRouterProvider, $ionicConfigProvider){

    // $ionicConfigProvider.views.maxCache(0);
    // $locationProvider.html5Mode(true);
    $ionicConfigProvider.backButton.previousTitleText(false).text('');
    
    $stateProvider

        
        .state('home', {
            url: '/',
            controller: 'HomeCtrl',
            templateUrl: 'views/home.html',
        })   

        // .state('test', {
        //     url: '/test',
        //     controller: 'HomeCtrl',
        //     templateUrl: 'views/home.html',
        // })

    $urlRouterProvider.otherwise('/');
};