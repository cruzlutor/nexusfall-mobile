var gulp    = require('gulp');
var sass    = require('gulp-sass');
var bs      = require('browser-sync').create('server');


gulp.task('sass', function () {
    return gulp.src('./src/css/main.sass')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./src/css/'))
        .pipe(bs.stream());
});

gulp.task('server', function() {
    bs.init({
        server: {
            baseDir: "./src/"
        }
    });

    gulp.watch("./src/css/**/*.sass", ['sass']);
    gulp.watch("./src/js/bundle.js").on('change', bs.reload);
    gulp.watch("./src/**/*.html").on("change", bs.reload);
});

gulp.task('develop', ['sass', 'server']);