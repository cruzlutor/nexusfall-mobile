/* jslint node:true */
'use strict';

var store = require('store');

function HomeCtrl($scope, $http, $ionicScrollDelegate) {

    $scope.data = {
        state: 0,
        error: null,
        server: store.get('region') || 'na',
        summonerName:store.get('summonerName') || '',
    };
    
    this.init = function(){
    }

    $scope.getMatch = function(){ 
        if($scope.data.summonerName != '' && $scope.data.server != ''){
            $scope.resetMatch();
            $http.get('http://nexusfall.com/match/'+$scope.data.server+'/'+$scope.data.summonerName).then($scope.onGet, $scope.onGetErr);
        }
    }

    $scope.resetMatch = function(){
        store.set('region', $scope.data.server);
        store.set('summonerName', $scope.data.summonerName);

        $ionicScrollDelegate.scrollTop();
        $scope.data.state = 1;
        $scope.teams = {
            blue:[],
            purple:[],
        };
    }

    $scope.onGet = function(response){
        $scope.data.state = 2;
        var serieTmp = [];
        var summoner;
        for(var player in response.data.players){
            summoner = response.data.players[player].summoner;
            summoner.ranked = (summoner.ranked) ? summoner.ranked : { tier: 'UNRANKED', points:0 };
            if(summoner.ranked.serie){
                serieTmp = summoner.ranked.serie;
                response.data.players[player].summoner.ranked.serie = [];
                for(var progress in serieTmp.progress){
                    response.data.players[player].summoner.ranked.serie.push(serieTmp.progress[progress]);
                }
            }

            if(response.data.players[player].team == 100){
                $scope.teams['blue'].push(response.data.players[player]);
            }else{
                $scope.teams['purple'].push(response.data.players[player]);
            }       
        }
    }

    $scope.onGetErr = function(response){
        $scope.data.state = 3;
        $scope.data.error = response.data.error;
    }
    
    this.init();
}

var ngModule = require('./');

ngModule.controller('HomeCtrl', ['$scope', '$http', '$ionicScrollDelegate', HomeCtrl]);